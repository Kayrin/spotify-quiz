const fs = require("fs"); 
const fetch = require('node-fetch');
const { URLSearchParams } = require('url');

const util = require('./util.js');
const spotifyParams = require('./spotify_params.json');
const appParams = require('../settings.json');
const SPOTIFY_PARAMS_FILEPATH = 'lib/spotify_params.json';

/**
 * SPOTIFY
 */
const nbTracksTotal = 20;
const clientId = appParams.spotify.clientId;
const clientSecret = appParams.spotify.clientSecret;
const requestParams = new URLSearchParams();
requestParams.append('grant_type', 'client_credentials');

// Avoid regenerating a Spotify API token if the previous one is still active (see getPlaylist function)
let access_token = spotifyParams.access_token;

function fetchAccessToken() {
  return fetch('https://accounts.spotify.com/api/token', {
    method: 'POST',
    headers: { 
      'Authorization': 'Basic ' + (new Buffer.from(clientId + ':' + clientSecret).toString('base64')),
    },
    body: requestParams
  });
}

function fetchPlaylist(playlistId) {
  return fetch('https://api.spotify.com/v1/playlists/' + playlistId + '?fields=tracks', {
    method: 'GET',
    headers: { 
      'Authorization': 'Bearer ' + access_token,
    },
  });
}

let tracks = [];
let selectedTracks = [];
let offset, limit;

async function fetchTracks(tracksData, tracksToSelect) {
  nextOffset = tracksData.offset + tracksData.limit;
  currentOffset = tracksData.offset;
  limit = tracksData.limit;

  nbFetch = Math.floor(tracksData.total / limit);
  console.log("nbFetch " + nbFetch);

  tracks = tracks.concat(tracksData.items);
  // console.log("tracks = " + tracks);
  let addTracks = () => {
    return fetch(tracksData.next, {
      method: 'GET',
      headers: { 
        'Authorization': 'Bearer ' + access_token,
      },
    })
    .then(res => res.json())
    .then(json => {
      tracks = tracks.concat(json.items);
    });
  }

  let asyncFor = async _ => {
    for (let i = 0; i < nbFetch; i++) {
      await addTracks();
      console.log("Done fetching tracks");
    }

    let intArray = [];
    let r;
    while (intArray.length < tracksToSelect){
      r = util.getRandomIntBetween(0, tracksData.total);
      // console.log("r = " + r);
      // console.log(tracks[r]);
      if(intArray.indexOf(r) === -1 && tracks[r].track.preview_url !== null) {
        intArray.push(r);
        selectedTracks.push(tracks[r]);
      }
    }
    console.log(intArray);
    console.log(selectedTracks);
    return selectedTracks;
  }
  return await asyncFor();
}

function getPlaylist (playlistId) {
  return new Promise ((resolve, reject) => {
    let tokenExpired = (new Date().getTime()) - spotifyParams.timestamp  > 3600000;
    if (tokenExpired || access_token === '') {
      fetchAccessToken()
      .then(res => res.json())
      .then(json => {
        console.log(json);
        spotifyParams.access_token = json.access_token;
        spotifyParams.timestamp = new Date().getTime();
        fs.writeFile(SPOTIFY_PARAMS_FILEPATH, JSON.stringify(spotifyParams, null, 2), err => {
          if (err) throw err;  
          console.log("Done writing");
        }); 
        access_token = json.access_token;
  
        fetchPlaylist(playlistId)
        .then(res => res.json())
        .then(json => {
          fetchTracks(json.tracks, nbTracksTotal).then(selectedTracks => {
            resolve(selectedTracks);
          });
        });
      });
    }
    else {
      console.log("Using current access token: " + access_token);
      fetchPlaylist(playlistId)
      .then(res => res.json())
      .then(json => {
        fetchTracks(json.tracks, nbTracksTotal).then(selectedTracks => {
          resolve(selectedTracks);
        });
      });
    }
  });
}

exports.requestPlaylist = (playlistId) => {
  // reset
  tracks = [];
  selectedTracks = [];
  offset, limit = 0;
  return getPlaylist(playlistId);
}