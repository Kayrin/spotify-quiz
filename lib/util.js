/**
 * Returns a random number between min (inclusive) and max (exclusive)
 */
exports.getRandomIntBetween = (min, max) => {  
  return Math.trunc(
    Math.random() * (max - min) + min
  )
}