## Init settings file

Create a `settings.json` at root level

```json
{
  "spotify": {
    "clientId": <your client ID>,
    "clientSecret": <your client secret>
  }
}
```

## Launch the app

Launch server `node app.js`

Access the website at `localhost:4242`

