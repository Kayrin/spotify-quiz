var songHistory = document.getElementById('song-history');
var chatInput = document.getElementById('song-input');
var chatForm = document.getElementById('song-input-form');
var playlistIdForm = document.getElementById('playlist-id-form');
var resetButton = document.getElementById('playlist-reset-btn');
var playerScore = document.getElementById('player-score');
var playerList = document.getElementById('player-list');
var guessArtistInfo = document.getElementById('guess-artist-info');
var guessSongInfo = document.getElementById('guess-song-info');
var progressBar = document.getElementById('song-progress-bar');
var songNumber = document.getElementById('song-number');
var songTotalNumber = document.getElementById('song-total-number');

const socket = io();
var typing = false;
var songPlaying = false;
var guessedArtist = false;
var guessedSong = false;
var audio = new Audio();
var currentTrackData;

// --------------------
//        UX
// --------------------

document.onready = (e) => {
  chatInput.focus();
}

document.addEventListener('DOMContentLoaded', function () {
  document.getElementById('song-input').addEventListener('focus', function () {
    typing = true;
  });
  document.getElementById('song-input').addEventListener('blur', function () {
    typing = false;
  });
});

document.onkeyup = function (event) {
  //user pressed and released enter key
  if (event.keyCode === 13) {
    if (!typing) {
      //user is not already typing, focus our chat text form
      chatInput.focus();
    }
  }
}

// --------------------
//    PLAYERS/SCORES
// --------------------

// Update page to current game state
socket.on('userRegistered', (username, trackNumber, pastTracks, isGameOn) => {
  document.getElementById("current-player-name").textContent = username;
  songNumber.textContent = trackNumber + 1;
  pastTracks.forEach(element => {
    var trackData = element.track;
    var releaseYear = trackData.album.release_date.substring(0, 4);
    songHistory.innerHTML += `
    <div class="song-entry">
      <div>
        <img src="${trackData.album.images[0].url}" class="song-album-cover">
      </div>
      <div>
        <div class="song-artist">${trackData.artists[0].name}</div>
        <div class="song-title">${trackData.name}</div>
        <div class="song-release-date">${releaseYear}</div>
      </div>
    </div>
    `;
  });
  if (isGameOn) {
    playlistIdForm.style.visibility = 'hidden';
  }
});

updateAllScores = (scores) => {
  for (var player in scores) {
    var playerListEntry = document.getElementById('player-list-entry-' + player);
    if (playerListEntry !== null) {
      // update score
      playerListEntry.children[1].textContent = scores[player];
    }
    else {
      playerList.innerHTML += `
      <div class="player-list-entry" id="player-list-entry-${player}">
        <span class="player-list-info player-list-name">${player}</span>
        <span class="player-list-info player-list-score">${scores[player]}</span>
      </div>
      `;
    }
  }
};

// Update scores list
socket.on('updateScorePlayers', (scores) => {
  updateAllScores(scores);
});

// Remove a disconnected player from the scores list
socket.on('removePlayer', (playerId) => {
  var childToDelete = document.getElementById('player-list-entry-' + playerId);
  playerList.removeChild(childToDelete);
});

socket.on('gameStarted', (totalSongs) => {
  playlistIdForm.style.visibility = 'hidden';
  resetButton.style.visibility = 'hidden';
  songTotalNumber.textContent = totalSongs;
});

// INPUT AND SCORE
chatForm.onsubmit = function (e) {
  //prevent the form from refreshing the page
  e.preventDefault();
  checkResponse(chatInput.value);
  chatInput.value = "";
}

function checkResponse(input) {
  console.log('Input: ' + input);
  // FIX: point added after timeout
  var inputValue = input.trim().toLowerCase().replace('’', '\'');
  if (!guessedArtist && inputValue === currentTrackData.artists[0].name.toLowerCase().replace('’', '\'').split('(')[0].trim()) {
    guessArtistInfo.style.visibility = 'visible';
    socket.emit('addScorePoint');
  }
  if (!guessedSong && inputValue === currentTrackData.name.toLowerCase().replace('’', '\'').split('(')[0].trim()) {
    guessSongInfo.style.visibility = 'visible';
    socket.emit('addScorePoint');
  }
}

socket.on('updateScore', (newScore) => {
  console.log("update score");
  playerScore.textContent = newScore;
});

// --------------------
//        TRACKS
// --------------------

document.getElementById('playlist-id-form').onsubmit = (e) => {
  e.preventDefault();
  socket.emit('startQuiz', document.getElementById('playlist-id-input').value);
}

document.getElementById('playlist-reset-btn').onclick = (e) => {
  e.preventDefault();
  socket.emit('resetQuiz');
}

socket.on('resetQuizClient', function () {
  songHistory.innerHTML = "";
  songNumber.textContent =  1;
});

// Play next track sent from server
socket.on('playTrack', function (trackNumber, trackData) {
  // reset
  guessArtistInfo.style.visibility = 'hidden';
  guessedArtist = false;
  guessSongInfo.style.visibility = 'hidden';
  guessedSong = false;
  songNumber.textContent = trackNumber + 1;

  console.log('play track');
  currentTrackData = trackData;
  audio = new Audio(trackData.preview_url);
  audio.volume = 0.15;
  try {
    audio.play();
  } catch (e) {
    console.log(e);
  }
  audio.onended = function(e) { 
    addSongToHistory(currentTrackData);
  };
  audio.ontimeupdate = function (e) {
    progressBar.style.width = (this.currentTime / this.duration * 100) + "%";
  };
  songPlaying = true;
  
  socket.emit('trackIsPlaying');
});

function addSongToHistory (trackData) {
  songPlaying = false;
  // console.log(trackData);
  var releaseYear = trackData.album.release_date.substring(0, 4);
  songHistory.innerHTML += `
  <div class="song-entry">
    <div>
      <img src="${trackData.album.images[0].url}" class="song-album-cover">
    </div>
    <div>
      <div class="song-artist">${trackData.artists[0].name}</div>
      <div class="song-title">${trackData.name}</div>
      <div class="song-release-date">${releaseYear}</div>
    </div>
  </div>
  `;
  socket.emit('triggerNextTrack');

  setTimeout(function () {
    socket.emit('requestNextTrack');
  }, 2000);
}

socket.on('endGame', function () {
  playlistIdForm.style.visibility = 'visible'
  resetButton.style.visibility = 'visible';
  guessArtistInfo.style.visibility = 'hidden';
  guessSongInfo.style.visibility = 'hidden';
});