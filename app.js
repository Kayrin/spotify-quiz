const express = require('express');
const app = express();

const server = require('http').createServer(app);
var io = require('socket.io')(server);

const spotify = require('./lib/spotify');
const util = require('./lib/util');

var session = require('express-session')({
  secret: 'my-secret',
  resave: true,
  saveUninitialized: true
});
var sharedsession = require("express-socket.io-session");

app.use('/client', express.static(__dirname + '/client'));

// Use express-session middleware for express
app.use(session);
 
// Use shared session middleware for socket.io
// setting autoSave:true
io.use(sharedsession(session, {
    autoSave:true
})); 

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/client/index.html');
});

console.log("Server started.");

// Game state variables
let loadedTracks = [];
let pastTracks = [];
let currentTrack = 0;
let nextTrackTriggered = false;
let isGameOn = false;

let SOCKET_LIST = {};
let SCORES = {};
let RANK = [];

const resetGame = (socket) => {
  // end game
  for (var i in SOCKET_LIST) {
    SCORES = {};
    SCORES[socket.id] = 0;
    currentTrack = 0;
    loadedTracks, pastTracks = [];

    socket.emit('updateScore', SCORES[socket.id]);
    SOCKET_LIST[i].emit('updateScorePlayers', SCORES);
  }
};

/**
 * Triggered when an user accesses the page (page reload)
 */
io.sockets.on('connection', function (socket) {
  /**
   * Generate a unique identifier for the socket on first connection.
   * It will be re-used everytime the same user reloads the page.
   */
  var socketId = socket.handshake.session.username;
  console.log('connect session: ' + socketId);

  if (!socketId) {
    var socketId = 'banana' + util.getRandomIntBetween(100, 999).toString();
    if (socketId in Object.keys(SOCKET_LIST)) {
      socketId = 'banana' + util.getRandomIntBetween(100, 999).toString();
    }
    // Save the identifier in the user's session
    socket.handshake.session.username = socketId;
    socket.handshake.session.save();
    console.log('User registered: ' + socketId);
  }

  // Register the user in the socket list and the scores list
  socket.id = socketId;
  SOCKET_LIST[socketId] = socket;
  SCORES[socketId] = 0;

  // Update page with current game state
  socket.emit('userRegistered', socketId, currentTrack, pastTracks, isGameOn);
  for (var i in SOCKET_LIST) {
    SOCKET_LIST[i].emit('updateScorePlayers', SCORES);
  }

  /**
   * Triggered when a user clicks on the START button
   */
  socket.on('startQuiz', (playlistId) => {
    // Check consistency in regards to Spotify playlist ID
    console.log('playlist ID: ' + playlistId);
    if (!playlistId || playlistId === "" || playlistId.length != 22) {
      console.log('Error: Empty or incorrect playlist ID')
    }
    else {
      // Reset game
      resetGame(socket);
      isGameOn = true;

      // Retrieve random songs from Spotify API and start the game
      console.log('Request track');
      spotify.requestPlaylist(playlistId).then(selectedTracks => {
        loadedTracks = selectedTracks;
        console.log(loadedTracks[currentTrack].track.artists[0].name + ' - ' + loadedTracks[currentTrack].track.name);
        for (var i in SOCKET_LIST) {
          SOCKET_LIST[i].emit('gameStarted', loadedTracks.length);
          SOCKET_LIST[i].emit('playTrack', currentTrack, loadedTracks[currentTrack].track);
        }
      });
    }
  });

  /**
   * Triggered after the last played song has ended
   * nextTrackTriggered is used to avoid concurrent access
   */
  socket.on('triggerNextTrack', () => {
    if (!nextTrackTriggered) {
      pastTracks.push(loadedTracks[currentTrack]);
      currentTrack++;
      nextTrackTriggered = true;
    }
  });

  /**
   * Triggered to reset the variable and avoid concurrent access
   */
  socket.on('trackIsPlaying', () => {
    nextTrackTriggered = false;
  });

  /**
   * Triggered 2 secs after the last played song has ended
   * Sends next track or ends the game
   */
  socket.on('requestNextTrack', function () {
    if (currentTrack < loadedTracks.length) {
      console.log('next track ' + currentTrack);
      console.log(loadedTracks[currentTrack].track.artists[0].name + ' - ' + loadedTracks[currentTrack].track.name);
      socket.emit('playTrack', currentTrack, loadedTracks[currentTrack].track);
    }
    else {
      console.log('Game ended');
      socket.emit('endGame');
    }
  });

  /**
   * Update scores server-side after a right answer
   */
  socket.on('addScorePoint', function() {
    if (!SCORES[socket.id])
      SCORES[socket.id] = 1;
    else 
      SCORES[socket.id]++;

    // Update own score client-side
    console.log("added point to " + socket.id);
    socket.emit('updateScore', SCORES[socket.id]);

    RANK = Object.keys(SCORES).sort((a, b) => SCORES[b] - SCORES[a]); // unused variable
    // Update all scores client-side
    for (var i in SOCKET_LIST) {
      SOCKET_LIST[i].emit('updateScorePlayers', SCORES);
    }
  })

  socket.on('resetQuiz', function() {
    resetGame(socket);
    for (var i in SOCKET_LIST) {
      SOCKET_LIST[i].emit('resetQuizClient');
    }
    isGameOn = false;
  });

  /**
   * Triggered when the user closes the page
   */
  socket.on('disconnect', function () {
    delete SOCKET_LIST[socket.id];
    delete SCORES[socket.id];
    for (var i in SOCKET_LIST) {
      SOCKET_LIST[i].emit('removePlayer', socket.id);
    }
  });
});
server.listen(4242);